#ifndef BASKET_VECTOR_2_HPP
#define BASKET_VECTOR_2_HPP

#include "_impl/_vector2_definition.hpp"
#include "_impl/_vector2_operators.hpp"
#include "_impl/_vector2_functions.hpp"
#include "_impl/_vector2_stream.hpp"

#endif