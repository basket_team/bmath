#ifndef BASKET_QUATERNION_HPP
#define BASKET_QUATERNION_HPP

#include "_impl/_quaternion_definition.hpp"
#include "_impl/_quaternion_operators.hpp"
#include "_impl/_quaternion_functions.hpp"

#endif
