#include "functions.hpp"

using namespace basket;

// It appears that there are linking errors with UUID.cpp
namespace _internal
{
    std::random_device device;
    std::mt19937_64 generator(device());
    std::uniform_int_distribution<uint64_t> distribution(std::numeric_limits<uint64_t>::min(), std::numeric_limits<uint64_t>::max());
    std::uniform_real_distribution<float> float_distribution(-1, 1);
    std::uniform_real_distribution<double> double_distribution(-1, 1);
}


float math::random()
{
    return _internal::float_distribution(_internal::generator);
}