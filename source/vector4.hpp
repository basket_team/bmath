#ifndef BASKET_VECTOR_4_HPP
#define BASKET_VECTOR_4_HPP

#include "_impl/_vector4_definition.hpp"
#include "_impl/_vector4_operators.hpp"
#include "_impl/_vector4_functions.hpp"
#include "_impl/_vector4_stream.hpp"

#endif
