#ifndef BASKET_MATH_CONSTANTS_HPP
#define BASKET_MATH_CONSTANTS_HPP

// #include <core/constraints.hpp>
// #include <core/pch.hpp>

namespace basket::bmath
{
    namespace _internal
    {
        template<typename T>
        constexpr T PI_Template = (T)3.14159265358979323846;
        // constexpr T PI_Template = T();

        template<typename T>
        constexpr T Tau_Template = T(2) * T(PI_Template<T>);

        template<typename T>
        constexpr T Root2_Template = T(1.41421356237309504880168872420969807856967187537694);
        template<typename T>
        constexpr T RootOf3_Template = T(1.7320508075);

        template<typename T>
        constexpr T Cos60_Template = T(1) / T(2);
        template<typename T>
        constexpr T Cos45_Template = Root2_Template<T> / T(2);
        template<typename T>
        constexpr T Cos30_Template = RootOf3_Template<T> / T(2);

        template<typename T>
        constexpr T Sin60_Template = Cos30_Template<T>;
        template<typename T>
        constexpr T Sin45_Template = Cos45_Template<T>;
        template<typename T>
        constexpr T Sin30_Template = Cos60_Template<T>;
    }

    constexpr double pi = _internal::PI_Template<double>;
    constexpr float tau = _internal::Tau_Template<float>;

    constexpr float root_of_2 = _internal::Root2_Template<float>;
    constexpr float root_of_3 = _internal::RootOf3_Template<float>;

    constexpr float cos_60 = _internal::Cos60_Template<float>;
    constexpr float cos_45 = _internal::Cos45_Template<float>;
    constexpr float cos_30 = _internal::Cos30_Template<float>;

    constexpr float sin_60 = _internal::Sin60_Template<float>;
    constexpr float sin_45 = _internal::Sin45_Template<float>;
    constexpr float sin_30 = _internal::Sin30_Template<float>;
}

#endif
