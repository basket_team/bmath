#ifndef BASKET_FUNCTIONS_HPP
#define BASKET_FUNCTIONS_HPP


// #include <core/constraints.hpp>
#include "constants.hpp"

namespace basket::bmath
{

    

    // template<double>
    // double random()
    // {
    //     return _internal::double_distribution(_internal::generator);
    // }

    template<typename T>
    constexpr T rad_to_deg(const T p_value)
    {
        return p_value * (T)180 / (T)bmath::pi;
    }

    template<typename T>
    constexpr T deg_to_rad(const T p_value)
    {
        return p_value * (T)bmath::pi / (T)180;
    }


    template<concept_numeric T>
    constexpr T clamp(const T p_value, const T p_min, const T p_max)
    {
        return p_value < p_min ? p_min :
                p_value > p_max ? p_max : p_value;
    }

    template<concept_numeric T>
    constexpr T abs(const T value)
    {
        return value < T(0) ? -value : value;
    }

    template<concept_numeric T>
    constexpr T sign(const T p_value)
    {
        return p_value < T(0) ? T(-1) : T(1);
    }

    

    
    // constexpr float min (const float p_lhs, const float p_rhs)
    // {
    //     return p_lhs < p_rhs ? p_lhs : p_rhs;
    // }

    // constexpr float max (const float p_lhs, const float p_rhs)
    // {
    //     return p_lhs > p_rhs ? p_lhs : p_rhs;
    // }

    // float random();
}

#endif