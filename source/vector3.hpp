#ifndef BASKET_VECTOR_3_HPP
#define BASKET_VECTOR_3_HPP


#include "_impl/_vector3_definition.hpp"
#include "_impl/_vector3_operators.hpp"
#include "_impl/_vector3_functions.hpp"
#include "_impl/_vector3_stream.hpp"

#endif