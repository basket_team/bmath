#ifndef BASKET__MATRIX22_DEFINITION_HPP
#define BASKET__MATRIX22_DEFINITION_HPP

// #include <core/typedefs.hpp>
#include "_impl/_vector2_definition.hpp"

namespace basket
{
    template<typename T>
    struct _Matrix22_Template
    {
        using value_type = T;
        using row_type = VectorXY_Template<value_type>;
        row_type row0;
        row_type row1;

        static const _Matrix22_Template<T> identity;
        static const _Matrix22_Template<T> zero;

        constexpr _Matrix22_Template<T>() = default;
        constexpr _Matrix22_Template<T>(const _Matrix22_Template<T>&) = default;
        constexpr _Matrix22_Template<T>(_Matrix22_Template<T>&&) = default;
        _Matrix22_Template<T>& operator=(const _Matrix22_Template<T>&) = default;
        _Matrix22_Template<T>& operator=(_Matrix22_Template<T>&&) = default;

        constexpr _Matrix22_Template(const T p_value): row0(p_value), row1(p_value) {}
        constexpr _Matrix22_Template(const row_type& p_row0, const row_type& p_row1): row0(p_row0), row1(p_row1) {}
        constexpr _Matrix22_Template(T p_0, T p_1, T p_2, T p_3): row0(p_0, p_1), row1(p_2, p_3) {}
    };

    template<typename T>
    const _Matrix22_Template<T> _Matrix22_Template<T>::identity{{1, 0}, {0, 1}};
    template<typename T>
    const _Matrix22_Template<T> _Matrix22_Template<T>::zero{{0, 0}, {0, 0}};

    

    using Matrix22 = _Matrix22_Template<float>;
}



#endif