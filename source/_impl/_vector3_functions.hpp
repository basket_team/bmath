#ifndef BASKET__VECTOR3_FUNCTIONS_HPP
#define BASKET__VECTOR3_FUNCTIONS_HPP

#include "_vector3_definition.hpp"
#include "_vector3_operators.hpp"
#include "_matrix33_functions.hpp"
#include <core/math/functions.hpp>
// #include "_quaternion_operators.hpp"
// #include "_quaternion_functions.hpp"

namespace basket::bmath
{

    
    constexpr Vector3::value_type dot(const Vector3& lhs, const Vector3& rhs)
    {
        return rhs.x * lhs.x + rhs.y * lhs.y + rhs.z * lhs.z;
    }


    constexpr Vector3 cross(const Vector3& p_left, const Vector3& p_right)
    {
        return Vector3(
            p_left.y * p_right.z - p_left.z * p_right.y,
            p_left.z * p_right.x - p_left.x * p_right.z,
            p_left.x * p_right.y - p_left.y * p_right.x
        );
    }


    constexpr Vector3::value_type distance_square(const Vector3& lhs, const Vector3& rhs)
    {
        float x = lhs.x - rhs.x;
        float y = lhs.y - rhs.y;
        float z = lhs.z - rhs.z;

        return x * x + y * y + z * z;
    }

    
    inline Vector3::value_type distance(const Vector3& lhs, const Vector3& rhs)
    {
        return sqrt(distance_square(lhs, rhs));
    }

    
    inline Vector3::value_type magnitude(const Vector3& v)
    {
        return static_cast<Vector3::value_type>(sqrt(v.x * v.x + v.y * v.y + v.z * v.z));
    }

    inline Vector3 normalized(const Vector3& v)
    {
        const Vector3::value_type m = magnitude(v);
        return v / m;
    }

    
    inline bool is_normalized(const Vector3& v)
    {
        return magnitude(v) == 1.0f;
    }

    
    inline void normalize(Vector3& v)
    {
        v = normalized(v);
    }

    
    constexpr Vector3 rotated(const Vector3& p_vector, const Vector3& p_axis, const Vector3::value_type p_angle)
    {
        return rotated(p_angle, p_axis) * p_vector;
    }

    constexpr Vector3 rotated_x(const Vector3& p_vector, const Vector3::value_type p_angle)
    {
        return rotated_x(p_angle) * p_vector;
    }

    constexpr Vector3 rotated_y(const Vector3& p_vector, const Vector3::value_type p_angle)
    {
        return rotated_y(p_angle) * p_vector;
    }

    constexpr Vector3 rotated_z(const Vector3& p_vector, const Vector3::value_type p_angle)
    {
        return rotated_z(p_angle) * p_vector;
    }

    // template<>
    // constexpr Vector3 rotated(const Vector3& p_vector, const Vector3& p_axis, const Vector3::value_type p_angle)
    // {
    //     return rotated(p_angle, p_axis) * p_vector;
    // }

    
    inline Vector3 projection(const Vector3& lhs, const Vector3& rhs)
    {
        float m = magnitude(rhs);
        return dot(lhs, rhs) * rhs / (m * m);
    }

    
    constexpr Vector3 projection_normal(const Vector3& lhs, const Vector3& rhs)
    {
        return dot(lhs, rhs) * rhs;
    }

    constexpr Vector3 scaled(const Vector3& p_vector, const Vector3& p_axis, const Vector3::value_type p_value)
    {
        return scaled(p_value, p_axis) * p_vector;
    }

    // inline Vector3 rotated2(const Vector3& p_vector, Vector3::value_type p_angle, const Vector3& p_axis)
    // {
    //     const Quaternion p{0, p_vector.x, p_vector.y, p_vector.z};
    //     const Vector3::value_type s = sin(p_angle * 0.5);
    //     const Quaternion q{cos(p_angle * 0.5f), p_axis.x * s, p_axis.y * s, p_axis.z * s};

    //     Quaternion r = q * p * inversed(q);
    //     return {r.x, r.y, r.z};
    // }

    
    // template<typename T>
    // std::ostream& operator<< (std::ostream& ss, const Vector3& p_vector)
    // {
    //     ss << "(" << p_vector.x << ", " << p_vector.y << ", " << p_vector.z << ")";
    //     return ss;
    // }
}

#endif