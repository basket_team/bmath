#ifndef BASKET__MATRIX44_DEFINITION_HPP
#define BASKET__MATRIX44_DEFINITION_HPP

#include "_vector4_definition.hpp"

namespace basket::bmath
{
    template<typename T>
    struct _Matrix44_Template
    {
        using value_type = T;
        using row_type = VectorXYZW_Template<T>;
        row_type row0;
        row_type row1;
        row_type row2;
        row_type row3;

        constexpr _Matrix44_Template() = default;
        constexpr _Matrix44_Template(const _Matrix44_Template<T>&) = default;
        constexpr _Matrix44_Template(_Matrix44_Template<T>&&) = default;

        constexpr _Matrix44_Template<T>& operator=(const _Matrix44_Template<T>&) = default;
        constexpr _Matrix44_Template<T>& operator=(_Matrix44_Template<T>&&) = default;

        constexpr _Matrix44_Template(const T p_value) : 
            row0(p_value, 0, 0, 0),
            row1(0, p_value, 0, 0),
            row2(0, 0, p_value, 0),
            row3(0, 0, 0, p_value)
        {

        }

        constexpr _Matrix44_Template(const row_type& p_row0, const row_type& p_row1, const row_type& p_row2, const row_type& p_row3):
            row0(p_row0),
            row1(p_row1),
            row2(p_row2),
            row3(p_row3)
            {}
        
        constexpr _Matrix44_Template(
            const value_type p_0, const value_type p_1, const value_type p_2, const value_type p_3,
            const value_type p_4, const value_type p_5, const value_type p_6, const value_type p_7,
            const value_type p_8, const value_type p_9, const value_type p_10, const value_type p_11,
            const value_type p_12, const value_type p_13, const value_type p_14, const value_type p_15
        ):
            row0{p_0, p_1, p_2, p_3},
            row1{p_4, p_5, p_6, p_7},
            row2{p_8, p_9, p_10, p_11},
            row3{p_12, p_13, p_14, p_15}
        {}

        row_type& operator[](uint8_t index)
        {
            switch(index)
            {
                case 0:
                    return row0;
                case 1:
                    return row1;
                case 2:
                    return row2;
                case 3:
                    return row3;
            }

            return row0; // I have no idea what else should I return in this case.
        }

        static const _Matrix44_Template<T> identity;
        static const _Matrix44_Template<T> one;

        
    };

    template<typename T>
    const _Matrix44_Template<T> _Matrix44_Template<T>::identity = {
        {T(1), 0, 0, 0},
        {0, T(1), 0, 0},
        {0, 0, T(1), 0},
        {0, 0, 0, T(1)}
    };

    template<typename T>
    const _Matrix44_Template<T> _Matrix44_Template<T>::one = {
        {1, 1, 1, 1},
        {1, 1, 1, 1},
        {1, 1, 1, 1},
        {1, 1, 1, 1}
    };


    using Matrix44 = _Matrix44_Template<float>;

    static_assert(sizeof(Matrix44) == sizeof(Matrix44::value_type) * 16);    
}

#endif