#ifndef BASKET__MATRIX22_OPERATORS_HPP
#define BASKET__MATRIX22_OPERATORS_HPP

#include "_matrix22_definition.hpp"

#include "_impl/_vector2_functions.hpp"
#include "_impl/_vector2_operators.hpp"


constexpr bool operator==(const basket::Matrix22& p_left, const basket::Matrix22& p_right)
{
    return p_left.row0 == p_right.row0 && p_left.row1 == p_right.row1;
}


constexpr bool operator!=(const basket::Matrix22& p_left, const basket::Matrix22& p_right)
{
    return !(p_left == p_right);
}


constexpr basket::Matrix22 operator-(const basket::Matrix22& p_value)
{
    basket::Matrix22 v(p_value);
    -v.row0;
    -v.row1;

    return v;
}


constexpr basket::Matrix22 operator+(const basket::Matrix22& p_value)
{
    return p_value;
}


constexpr basket::Matrix22 operator+(const basket::Matrix22& p_left, const basket::Matrix22& p_right)
{
    return basket::Matrix22{
        {p_left.row0 + p_right.row0},
        {p_left.row1 + p_right.row1}
    };
}


constexpr basket::Matrix22 operator-(const basket::Matrix22& p_left, const basket::Matrix22& p_right)
{
    return basket::Matrix22{
        {p_left.row0 - p_right.row0},
        {p_left.row1 - p_right.row1}
    };
}


constexpr basket::Matrix22 operator*(const basket::Matrix22& p_matrix, const basket::Matrix22::value_type p_value)
{
    return basket::Matrix22(p_value * p_matrix.row0, p_value * p_matrix.row1);
}



constexpr basket::Matrix22 operator*(const basket::Matrix22::value_type p_value, const basket::Matrix22& p_matrix)
{
    return p_matrix * p_value;
}




constexpr basket::Matrix22 operator*(const basket::Matrix22& p_left, const basket::Matrix22& p_right)
{
    return basket::Matrix22{
        {basket::bmath::dot(p_left.row0, basket::Vector2(p_right.row0.x, p_right.row1.x)), basket::bmath::dot(p_left.row0, basket::Vector2(p_right.row0.y, p_right.row1.y))},
        {basket::bmath::dot(p_left.row1, basket::Vector2(p_right.row0.x, p_right.row1.x)), basket::bmath::dot(p_left.row1, basket::Vector2(p_right.row0.y, p_right.row1.y))}
    };
}



#endif