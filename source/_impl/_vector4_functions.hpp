#ifndef BASKET__VECTOR4_FUNCTIONS_HPP
#define BASKET__VECTOR4_FUNCTIONS_HPP

namespace basket::bmath
{

    
    constexpr Vector4::value_type dot(const Vector4& p_left, const Vector4& p_right)
    {
        return p_left.x * p_right.x + p_left.y * p_right.y + p_left.z * p_right.z + p_left.w * p_right.w;
    }

    
    constexpr Vector4::value_type distance_square(const Vector4& p_left, const Vector4& p_right)
    {
        using value_type = Vector4::value_type;
        const value_type diffx = p_left.x - p_right.x;
        const value_type diffy = p_left.y - p_right.y;
        const value_type diffz = p_left.z - p_right.z;
        const value_type diffw = p_left.w - p_right.w;

        return diffx * diffx + diffy * diffy + diffz * diffz + diffw * diffw;
    }


    inline Vector4::value_type distance(const Vector4& p_left, const Vector4& p_right)
    {
        return sqrt(distance_square(p_left, p_right));
    }

    
    constexpr Vector4::value_type magnitude_square(const Vector4& p_vector)
    {
        return p_vector.x * p_vector.x + p_vector.y * p_vector.y + p_vector.z * p_vector.z + p_vector.w * p_vector.w;
    }

    
    inline Vector4::value_type magnitude(const Vector4& p_vector)
    {
        return sqrt(magnitude_square(p_vector));
    }

    
    inline Vector4 normalized(const Vector4& p_vector)
    {
        Vector4::value_type m = magnitude(p_vector);
        return p_vector / m;
    }

    
    inline void normalize(Vector4& p_vector)
    {
        Vector4::value_type m = magnitude(p_vector);
        p_vector.x /= m;
        p_vector.y /= m;
        p_vector.z /= m;
        p_vector.w /= m;
    }



    
}

#endif