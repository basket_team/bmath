#ifndef BASKET__VECTOR2_FUNCTIONS_HPP
#define BASKET__VECTOR2_FUNCTIONS_HPP

#include "_vector2_definition.hpp"
#include "_vector2_operators.hpp"
#include "_polar2_definition.hpp"
// #include <core/math/functions.hpp>

#include "_vector3_definition.hpp"
// #include <core/math/_impl/_vector3_operators.hpp>

namespace basket
{
    template<typename T>
    struct Polar2_Template;
}

namespace basket::bmath
{
    constexpr Vector2::value_type dot(const Vector2& lhs, const Vector2& rhs)
    {
        return rhs.x * lhs.x + rhs.y * lhs.y;
    }

    constexpr Vector3 cross(const Vector2& p_left, const Vector2& p_right)
    {
        return Vector3(
            0,
            0,
            p_left.x * p_right.y - p_left.y * p_right.x
        );
    }

    
    
    constexpr Vector2::value_type distance_square(const Vector2& lhs, const Vector2& rhs)
    {
        float x = lhs.x - rhs.x;
        float y = lhs.y - rhs.y;

        return x * x + y * y;
    }

    
    inline Vector2::value_type distance(const Vector2& lhs, const Vector2& rhs)
    {
        return sqrt(distance_square(lhs, rhs));
    }

    inline Vector2::value_type magnitude(const Vector2& v)
    {
        return static_cast<Vector2::value_type>(sqrt(v.x * v.x + v.y * v.y));
    }

    
    inline Vector2::value_type magnitude_square(const Vector2& v)
    {
        return v.x * v.x + v.y * v.y;
    }

    constexpr Vector2 bounce(const Vector2& p_vector, const Vector2& p_normal)
    {
        return p_vector - p_normal * dot(p_normal, p_vector) * static_cast<Vector2::value_type>(2);
    }

    
    inline Vector2 normalized(const Vector2& v)
    {
        const Vector2::value_type m = magnitude(v);
        return v / m;
    }

    inline bool is_normalized(const Vector2& v)
    {
        return magnitude(v) == 1.0f;
    }

    
    inline void normalize(Vector2& v)
    {
        v = normalized(v);
    }

    

    inline Polar2 to_polar(const Vector2& p_vector)
    {
        return {magnitude(p_vector), atan2f(p_vector.y, p_vector.x)};
    }

    
    

    // template<concept_vector2 T>
    // std::ostream& operator<< (std::ostream& ss, const T& p_vector)
    // {
    //     ss << "(" << p_vector.x << ", " << p_vector.y << ")";
    //     return ss;
    // }
}

#endif