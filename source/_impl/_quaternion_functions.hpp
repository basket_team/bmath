#ifndef BASKET__QUATERNIONS_FUNCTIONS_HPP
#define BASKET__QUATERNIONS_FUNCTIONS_HPP

#include "_quaternion_definition.hpp"
#include "_quaternion_operators.hpp"
// #include <core/pch.hpp>

namespace basket::bmath
{
    constexpr Quaternion::value_type dot(const Quaternion& p_left, const Quaternion& p_right)
    {
        return p_left.x * p_right.x + p_left.y * p_right.y + p_left.z * p_right.z + p_left.w * p_right.w;
    }

    constexpr Quaternion conjugated(const Quaternion& p_quat)
    {
        return {p_quat.w, -p_quat.x, -p_quat.y, -p_quat.z};
    }

    constexpr Quaternion::value_type magnitude_square(const Quaternion& p_quat)
    {
        return p_quat.w * p_quat.w + p_quat.x * p_quat.x + p_quat.y * p_quat.y + p_quat.z * p_quat.z;
    }

    inline Quaternion::value_type magnitude(const Quaternion& p_quat)
    {
        return sqrt(magnitude_square(p_quat));
    }

    inline Quaternion inversed(const Quaternion& p_quat)
    {
        return conjugated(p_quat) / magnitude(p_quat);
    }

    inline Quaternion::value_type diff(const Quaternion& p_left, const Quaternion& p_right)
    {
        return (p_right * inversed(p_left)).w;
    }

    constexpr Quaternion log(const Quaternion& p_quat)
    {
        return {0, p_quat.x, p_quat.y, p_quat.z};
    }

    constexpr Quaternion exp(const Quaternion& p_quat)
    {
        return {0, p_quat.x, p_quat.y, p_quat.z};
    }
}


#endif