#ifndef BASKET__MATRIX33_FUNCTIONS_HPP
#define BASKET__MATRIX33_FUNCTIONS_HPP

#include "_matrix33_definition.hpp"
#include "_matrix33_operators.hpp"
#include "_matrix22_functions.hpp"

namespace basket::bmath
{
    
    constexpr Matrix33 transposed(const Matrix33& p_matrix) 
    {
        using row_type = Matrix33::row_type;
        const row_type& r0 = p_matrix.row0;
        const row_type& r1 = p_matrix.row1;
        const row_type& r2 = p_matrix.row2;

        return {
            {r0.x, r1.x, r2.x},
            {r0.y, r1.y, r2.y},
            {r0.z, r1.z, r2.z},
        };
    }

    
    // constexpr Matrix33::value_type determinant(const Matrix33& p_matrix)
    // {
    //     using row_type = Matrix33::row_type;
    //     return row_type::dot(p_matrix.row0, row_type::cross(p_matrix.row1, p_matrix.row2));
    // }

    
    constexpr Matrix33 rotated_x(const Matrix33::value_type p_angle)
    {
        using row_type = Matrix33::row_type;
        return {
            {row_type(1,    0,              0)},
            {row_type(0,    cos(p_angle),   sin(p_angle))},
            {row_type(0,    -sin(p_angle),  cos(p_angle))}
        };
    }

    
    constexpr Matrix33 rotated_y(const Matrix33::value_type p_angle)
    {
        using row_type = Matrix33::row_type;
        return {
            {row_type(cos(p_angle),     0,  -sin(p_angle))},
            {row_type(0,                1,              0)},
            {row_type(sin(p_angle),     0,  cos(p_angle))}
        };
    }

    
    constexpr Matrix33 rotated_z(const Matrix33::value_type p_angle)
    {
        using row_type = Matrix33::row_type;
        return {
            {row_type(cos(p_angle),     sin(p_angle),   0)},
            {row_type(-sin(p_angle),    cos(p_angle),   0)},
            {row_type(0,                0,              1)}
        };
    }

    // template<typename VT, typename MT = _Matrix33_Template<VT>, typename VET = _Matrix33_Template<VT>::row_type>
    inline Matrix33 rotated(const Matrix33::value_type p_angle, const Matrix33::row_type& p_axis)
    {
        using value_type = Matrix33::value_type;

        const value_type c = cos(p_angle);
        const value_type c_1 = (value_type)1 - c;
        const value_type s = sin(p_angle);

        return {
            {p_axis.x * p_axis.x * c_1 + c,             p_axis.x * p_axis.y * c_1 + p_axis.z * s,   p_axis.x * p_axis.z * c_1 - p_axis.y * s},
            {p_axis.x * p_axis.y * c_1 - p_axis.z * s,  p_axis.y * p_axis.y * c_1 + c,              p_axis.y * p_axis.z * c_1 + p_axis.x * s},
            {p_axis.x * p_axis.z * c_1 + p_axis.y * s,  p_axis.y * p_axis.z * c_1 - p_axis.x * s,   p_axis.z * p_axis.z * c_1 + c}
        };
    }

    
    constexpr Matrix33 scaled(const Matrix33& p_matrix, const typename Matrix33::row_type& p_value)
    {
        return {
            {p_matrix.row0.x * p_value.x,   p_matrix.row0.y,                p_matrix.row0.z},
            {p_matrix.row1.x,               p_matrix.row1.y * p_value.y,    p_matrix.row1.z},
            {p_matrix.row2.x,               p_matrix.row2.y,                p_matrix.row2.z * p_value.z}
        };
    }

    
    constexpr Matrix33 scaled(const typename Matrix33::row_type& p_value)
    {
        return {
            {p_value.x,     0,            0},
            {0,             p_value.y,    0},
            {0,             0,            p_value.z}
        };
    }

    
    constexpr Matrix33 scaled(const typename Matrix33::value_type p_value, const typename Matrix33::row_type& p_axis)
    {
        using value_type = const typename Matrix33::value_type;
        using row_type = const typename Matrix33::row_type;
        
        const value_type one = static_cast<value_type>(1);
        const value_type k_1 = p_value - one;
        const value_type x2 = p_axis.x * p_axis.x;
        const value_type y2 = p_axis.y * p_axis.y;
        const value_type z2 = p_axis.z * p_axis.z;

        return {
            {one + k_1 * x2, k_1 * p_axis.x * p_axis.y, k_1 * p_axis.x * p_axis.z},
            {k_1 * p_axis.x * p_axis.y, one + k_1 * y2,  k_1 * p_axis.y * p_axis.z},
            {k_1 * p_axis.x * p_axis.z, k_1 * p_axis.y * p_axis.z, one + k_1 * z2}
        };
    }

    constexpr Matrix33 ortho(const Vector3& p_plane)
    {
        return scaled(0, p_plane);
    }

    constexpr Matrix33 reflected(const Matrix33::row_type& p_axis)
    {
        return scaled(-1, p_axis);
    }

    // This function can be seen as a cross product.
    constexpr Matrix33::value_type determinant(const Matrix33& p_matrix)
    {
        return 
        p_matrix.row0.x * (p_matrix.row1.y * p_matrix.row2.z - p_matrix.row1.z * p_matrix.row2.y) -
        p_matrix.row0.y * (p_matrix.row1.x * p_matrix.row2.z - p_matrix.row1.z * p_matrix.row2.x) +
        p_matrix.row0.z * (p_matrix.row1.x * p_matrix.row2.y - p_matrix.row1.y * p_matrix.row2.x);
    }

    constexpr Matrix33 adjacent(const Matrix33& p_matrix)
    {
        return transposed({
            {
                determinant(Matrix22(p_matrix.row1.y, p_matrix.row1.z, p_matrix.row2.y, p_matrix.row2.z)),
                -determinant(Matrix22(p_matrix.row1.x, p_matrix.row1.z, p_matrix.row2.x, p_matrix.row2.z)),
                determinant(Matrix22(p_matrix.row1.x, p_matrix.row1.y, p_matrix.row2.x, p_matrix.row2.y))
            },
            {
                -determinant(Matrix22(p_matrix.row0.y, p_matrix.row0.z, p_matrix.row2.y, p_matrix.row2.z)),
                determinant(Matrix22(p_matrix.row0.x, p_matrix.row0.z, p_matrix.row2.x, p_matrix.row2.z)),
                -determinant(Matrix22(p_matrix.row0.x, p_matrix.row0.y, p_matrix.row2.x, p_matrix.row2.y))
            },
            {
                determinant(Matrix22(p_matrix.row0.y, p_matrix.row0.z, p_matrix.row1.y, p_matrix.row1.z)),
                -determinant(Matrix22(p_matrix.row0.x, p_matrix.row0.z, p_matrix.row1.x, p_matrix.row1.z)),
                determinant(Matrix22(p_matrix.row0.x, p_matrix.row0.y, p_matrix.row1.x, p_matrix.row1.y))
            }
        });
    }

    constexpr Matrix33 inversed(const Matrix33& p_matrix)
    {
        const Matrix33::value_type det = determinant(p_matrix);
        if (det == static_cast<Matrix33::value_type>(0))
        {
            return {};
        }

        return adjacent(p_matrix) / det;
    }
}

#endif