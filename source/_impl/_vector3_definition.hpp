#ifndef BASKET__VECTOR3_DEFINITION_HPP
#define BASKET__VECTOR3_DEFINITION_HPP

// #include <core/pch.hpp>
// #include <core/constraints.hpp>
#include <stdint.h>

namespace basket::bmath
{
    template<typename T>
    struct VectorXYZ_Template
    {
        using value_type = T;
        T x; /**< T x variable */
        T y; /**< T y variable */
        T z; /**< T z variable */

        /**
         * @brief Construct a new vector x y z object
         * 
         * @param p_x 
         * @param p_y 
         * @param p_z 
         */
        constexpr VectorXYZ_Template(T p_x, T p_y, T p_z): x{p_x}, y{p_y}, z{p_z} {}
        constexpr VectorXYZ_Template(const T value): VectorXYZ_Template(value, value, value) {}

        constexpr VectorXYZ_Template () = default;
        constexpr VectorXYZ_Template (const VectorXYZ_Template<T>&) = default;
        constexpr VectorXYZ_Template (VectorXYZ_Template<T>&&) = default;
        VectorXYZ_Template& operator=(const VectorXYZ_Template<T>&) = default;
        VectorXYZ_Template& operator=(VectorXYZ_Template<T>&&) = default;

    
        static const VectorXYZ_Template<T> zero;
        static const VectorXYZ_Template<T> one;
        static const VectorXYZ_Template<T> up;
        static const VectorXYZ_Template<T> down;
        static const VectorXYZ_Template<T> right;
        static const VectorXYZ_Template<T> left;
        static const VectorXYZ_Template<T> forward;
        static const VectorXYZ_Template<T> backward;


        T& operator[] (int p_index)
        {
            switch(p_index)
            {
                case 0:
                    return x;
                case 1:
                    return y;
                case 2:
                    return z;
            }

            return T(0);
        }

        
    };

    template<typename T>
    const VectorXYZ_Template<T> VectorXYZ_Template<T>::zero{0, 0, 0};

    template<typename T>
    const VectorXYZ_Template<T> VectorXYZ_Template<T>::one{(T)1, (T)1, (T)1};

    template<typename T>
    const VectorXYZ_Template<T> VectorXYZ_Template<T>::right{(T)1, 0, 0};

    template<typename T>
    const VectorXYZ_Template<T> VectorXYZ_Template<T>::left{(T)-1, 0, 0};

    template<typename T>
    const VectorXYZ_Template<T> VectorXYZ_Template<T>::forward{0, 0, (T)-1};

    template<typename T>
    const VectorXYZ_Template<T> VectorXYZ_Template<T>::backward{0, 0, (T)1};

    template<typename T>
    const VectorXYZ_Template<T> VectorXYZ_Template<T>::up{0, (T)1, 0};

    template<typename T>
    const VectorXYZ_Template<T> VectorXYZ_Template<T>::down{0, (T)-1, 0};

    using Vector3 = VectorXYZ_Template<float>;
    using Vector3i = VectorXYZ_Template<int64_t>;
    using Vector3ui = VectorXYZ_Template<uint64_t>;

    

    // template<typename T>
    // std::ostream& operator<< (std::ostream& ss, const VectorXYZ_Template<T>& p_vector)
    // {
    //     ss << "(" << p_vector.x << ", " << p_vector.y << ", " << p_vector.z << ")";
    //     return ss;
    // }
}

#endif