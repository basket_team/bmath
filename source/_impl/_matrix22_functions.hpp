#ifndef BASKET__MATRIX22_FUNCTIONS_HPP
#define BASKET__MATRIX22_FUNCTIONS_HPP

#include "_impl/_vector2_functions.hpp"
#include "_impl/_matrix22_definition.hpp"

namespace basket::bmath
{
    
    constexpr Matrix22 matrix22(const Matrix22::value_type p_angle)
    {
        return {
            {cos(p_angle), sin(p_angle)},
            {-sin(p_angle), cos(p_angle)}
        };
    }

    
    inline void rotation_add(Matrix22& p_matrix, const Matrix22::value_type p_angle)
    {
        p_matrix  = Matrix22 {
            {p_matrix.row0.x + cos(p_angle), p_matrix.row0.y + sin(p_angle)},
            {p_matrix.row1.x - sin(p_angle), p_matrix.row1.y + cos(p_angle)}
        };
    }

    
    inline Matrix22 rotation_added(const Matrix22& p_matrix, const Matrix22::value_type p_angle)
    {
        return {
            {p_matrix.row0.x + cos(p_angle), p_matrix.row0.y + sin(p_angle)},
            {p_matrix.row1.x - sin(p_angle), p_matrix.row1.y + cos(p_angle)}
        };
    }

    
    constexpr Matrix22 transposed(const Matrix22& p_value)
    {
        using row_type = Matrix22::row_type;
        const row_type& row0 = p_value.row0;
        const row_type& row1 = p_value.row1;

        return Matrix22{
            {row0.x, row1.x},
            {row0.y, row1.y},
        };
    }

    
    inline void transpose(Matrix22& p_value)
    {
        using row_type = Matrix22::row_type;
        Matrix22 copy(p_value);

        p_value.row0 = row_type(copy.row0.x, copy.row1.x);
        p_value.row1 = row_type(copy.row0.y, copy.row1.y);
    }

    constexpr Matrix22 scaled(const Matrix22::value_type p_value, const Matrix22::row_type& p_axis)
    {
        using value_type = Matrix22::value_type;
        const value_type one = static_cast<value_type>(1);
        const value_type k_1 = p_value - one;

        return {
            {one + k_1 * p_axis.x * p_axis.x, k_1 * p_axis.x * p_axis.y},
            {k_1 * p_axis.x * p_axis.y, one + k_1 * p_axis.y * p_axis.y}
        };
    }


    constexpr Matrix22 ortho(const Vector2& p_axis)
    {
        Vector2::value_type one = static_cast<Vector2::value_type>(1);
        return {
            {one - p_axis.x * p_axis.x, -p_axis.x * p_axis.y},
            {-p_axis.x * p_axis.y, one - p_axis.y * p_axis.y}
        };
    }

    constexpr Matrix22 reflected(const Matrix22::row_type& p_axis)
    {
        return scaled(static_cast<Matrix22::value_type>(-1), p_axis);
    }

    constexpr Matrix22::value_type determinant(const Matrix22& p_matrix)
    {
        return p_matrix.row0.x * p_matrix.row1.y - p_matrix.row0.y * p_matrix.row1.x;
    }

    
}

#endif