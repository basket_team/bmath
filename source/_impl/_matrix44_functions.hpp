#ifndef BASKET__MATRIX44_FUNCTIONS_HPP
#define BASKET__MATRIX44_FUNCTIONS_HPP


#include "_matrix44_definition.hpp"
#include "_matrix44_operators.hpp"
#include "_matrix33_definition.hpp"
#include "_matrix33_functions.hpp"
#include "_vector3_definition.hpp"
#include "_vector3_functions.hpp"
#include "_vector4_definition.hpp"
// #include <core/macros.hpp>

namespace basket::bmath
{

    constexpr Matrix44 _translated_cm(const basket::Matrix44 p_matrix, const Matrix33::row_type& p_vector)
    {
        return {
            {p_matrix.row0.x, p_matrix.row0.y, p_matrix.row0.z, p_matrix.row0.w + p_vector.x},
            {p_matrix.row1.x, p_matrix.row1.y, p_matrix.row1.z, p_matrix.row1.w + p_vector.y},
            {p_matrix.row2.x, p_matrix.row2.y, p_matrix.row2.z, p_matrix.row2.w + p_vector.z},
            {p_matrix.row3.x, p_matrix.row3.y, p_matrix.row3.z, p_matrix.row3.w}
        };
    }

    constexpr Matrix44 _translated_rm(const basket::Matrix44 p_matrix, const Matrix33::row_type& p_vector)
    {
        return {
            {p_matrix.row0.x,               p_matrix.row0.y,                p_matrix.row0.z,                p_matrix.row0.w},
            {p_matrix.row1.x,               p_matrix.row1.y,                p_matrix.row1.z,                p_matrix.row1.w},
            {p_matrix.row2.x,               p_matrix.row2.y,                p_matrix.row2.z,                p_matrix.row2.w},
            {p_matrix.row3.x + p_vector.x,  p_matrix.row3.y + p_vector.y,   p_matrix.row3.z + p_vector.z,   p_matrix.row3.w}
        };
    }
    
    constexpr basket::Matrix44 translated(const basket::Matrix44 p_matrix, const Matrix33::row_type& p_vector)
    {
        return _translated_rm(p_matrix, p_vector);
    }


    // inline Matrix44 rotated(Matrix44& p_matrix, const Matrix44::value_type p_angle, const Matrix33::row_type p_axis)
    // {
    //     Matrix33 m = rotated(p_angle, p_axis);

    //     p_matrix.row0 = Vector4(m.row0.x, m.row0.y, m.row0.z, p_matrix.row0.w);
    //     p_matrix.row1 = Vector4(m.row1.x, m.row1.y, m.row1.z, p_matrix.row0.w);
    //     p_matrix.row1 = Vector4(m.row2.x, m.row2.y, m.row2.z, p_matrix.row0.w);

    //     return p_matrix;
    // }
    
    constexpr Matrix44 transposed(const Matrix44& p_matrix)
    {
        return {
            {p_matrix.row0.x, p_matrix.row1.x, p_matrix.row2.x, p_matrix.row3.x},
            {p_matrix.row0.y, p_matrix.row1.y, p_matrix.row2.y, p_matrix.row3.y},
            {p_matrix.row0.z, p_matrix.row1.z, p_matrix.row2.z, p_matrix.row3.z},
            {p_matrix.row0.w, p_matrix.row1.w, p_matrix.row2.w, p_matrix.row3.w}
        };
    }

    constexpr void transpose(Matrix44& p_matrix)
    {
        const Matrix44 m(p_matrix);
        p_matrix.row0 = {m.row0.x, m.row1.x, m.row2.x, m.row3.x};
        p_matrix.row1 = {m.row0.y, m.row1.y, m.row2.y, m.row3.y};
        p_matrix.row2 = {m.row0.z, m.row1.z, m.row2.z, m.row3.z};
        p_matrix.row3 = {m.row0.w, m.row1.w, m.row2.w, m.row3.w};
    }

    constexpr Matrix44::value_type determinant(const Matrix44& p_matrix)
    {
        return p_matrix.row0.x * (
                    p_matrix.row1.y * (p_matrix.row2.z * p_matrix.row3.w - p_matrix.row2.w * p_matrix.row3.z) +
                    p_matrix.row1.z * (p_matrix.row2.w * p_matrix.row3.y - p_matrix.row2.y * p_matrix.row3.w) +
                    p_matrix.row1.w * (p_matrix.row2.y * p_matrix.row3.z - p_matrix.row2.z * p_matrix.row3.y)
                ) -
                p_matrix.row0.y * (
                    p_matrix.row1.x * (p_matrix.row2.z * p_matrix.row3.w - p_matrix.row2.w * p_matrix.row3.z) +
                    p_matrix.row1.z * (p_matrix.row2.w * p_matrix.row3.x - p_matrix.row2.x * p_matrix.row3.w) +
                    p_matrix.row1.w * (p_matrix.row2.x * p_matrix.row3.z - p_matrix.row2.z * p_matrix.row3.x)
                ) +
                p_matrix.row0.z * (
                    p_matrix.row1.x * (p_matrix.row2.y * p_matrix.row3.w - p_matrix.row2.w * p_matrix.row3.y) +
                    p_matrix.row1.y * (p_matrix.row2.w * p_matrix.row3.x - p_matrix.row2.x * p_matrix.row3.w) +
                    p_matrix.row1.w * (p_matrix.row2.x * p_matrix.row3.y - p_matrix.row2.y * p_matrix.row3.x)
                ) -
                p_matrix.row0.w * (
                    p_matrix.row1.x * (p_matrix.row2.y * p_matrix.row3.z - p_matrix.row2.w * p_matrix.row3.y) +
                    p_matrix.row1.y * (p_matrix.row2.z * p_matrix.row3.x - p_matrix.row2.x * p_matrix.row3.z) +
                    p_matrix.row1.z * (p_matrix.row2.x * p_matrix.row3.y - p_matrix.row2.y * p_matrix.row3.x)
                );
    }

    

    // inline Matrix33 rotated(const Matrix33::value_type p_angle, const Matrix33::row_type& p_axis)
    // {
    //     using value_type = Matrix33::value_type;

    //     const value_type c = cos(p_angle);
    //     const value_type c_1 = (value_type)1 - c;
    //     const value_type s = sin(p_angle);

    //     return {
    //         {p_axis.x * p_axis.x * c_1 + c,             p_axis.x * p_axis.y * c_1 + p_axis.z * s,   p_axis.x * p_axis.z * c_1 - p_axis.y * s},
    //         {p_axis.x * p_axis.y * c_1 - p_axis.z * s,  p_axis.y * p_axis.y * c_1 + c,              p_axis.y * p_axis.z * c_1 + p_axis.x * s},
    //         {p_axis.x * p_axis.z * c_1 + p_axis.y * s,  p_axis.y * p_axis.z * c_1 - p_axis.x * s,   p_axis.z * p_axis.z * c_1 + c}
    //     };
    // }

    BK_FORCEINLINE Matrix44 _rotated_rm(const Matrix44& p_matrix, const Matrix33::value_type p_angle, const Matrix33::row_type& p_axis)
    {
        using value_type = Matrix33::value_type;

        const value_type c = cos(p_angle);
        const value_type c_1 = (value_type)1 - c;
        const value_type s = sin(p_angle);

        return {
            {p_axis.x * p_axis.x * c_1 + c,             p_axis.x * p_axis.y * c_1 + p_axis.z * s,   p_axis.x * p_axis.z * c_1 - p_axis.y * s,   0},
            {p_axis.x * p_axis.y * c_1 - p_axis.z * s,  p_axis.y * p_axis.y * c_1 + c,              p_axis.y * p_axis.z * c_1 + p_axis.x * s,   0},
            {p_axis.x * p_axis.z * c_1 + p_axis.y * s,  p_axis.y * p_axis.z * c_1 - p_axis.x * s,   p_axis.z * p_axis.z * c_1 + c,              0},
            {p_matrix.row3.x,                           p_matrix.row3.y,                            p_matrix.row3.z,                            1}
        };
    }

    BK_FORCEINLINE Matrix44 _rotated_cm(const Matrix44& p_matrix, const Matrix44::value_type p_angle, const Matrix33::row_type& p_axis)
    {
        using value_type = Matrix33::value_type;

        const value_type c = cos(p_angle);
        const value_type c_1 = (value_type)1 - c;
        const value_type s = sin(p_angle);

        return {
            {p_axis.x * p_axis.x * c_1 + c,             p_axis.x * p_axis.y * c_1 - p_axis.z * s,   p_axis.x * p_axis.z * c_1 + p_axis.y * s,   p_matrix.row0.w},
            {p_axis.x * p_axis.y * c_1 + p_axis.z * s,  p_axis.y * p_axis.y * c_1 + c,              p_axis.y * p_axis.z * c_1 - p_axis.x * s,   p_matrix.row1.w},
            {p_axis.x * p_axis.z * c_1 - p_axis.y * s,  p_axis.y * p_axis.z * c_1 + p_axis.x * s,   p_axis.z * p_axis.z * c_1 + c,              p_matrix.row2.w},
            {0,                                         0,                                          0,                                          1}
        };
    }

    inline Matrix44 rotated(const Matrix44& p_matrix, const Matrix44::value_type p_angle, const Matrix33::row_type& p_axis)
    {
        return _rotated_cm(p_matrix, p_angle, p_axis);
    }


    BK_FORCEINLINE Matrix44 _perspective_rh01(const Matrix44::value_type p_fov, const Matrix44::value_type p_aspect_ratio, const Matrix44::value_type p_near, const Matrix44::value_type p_far)
    {
        using value_type = Matrix44::value_type;

        const value_type one = static_cast<value_type>(1);

        // const value_type fl = one / tan(p_fov * static_cast<value_type>(0.5));
        const value_type fl = one / tan(p_fov / static_cast<value_type>(2));
        // const value_type zx = one / (fl * p_aspect_ratio);
        // const value_type zy = one / fl;
        const value_type zx = fl / p_aspect_ratio;
        const value_type zy = fl;
        const value_type n = - p_far / (p_far - p_near);
        const value_type f = -(p_near * p_far) / (p_far - p_near);

        return {
            {zx, 0, 0, 0},
            {0, zy, 0, 0},
            {0, 0, n, -one},
            {0, 0, f, 0}
        };
    }


    inline Matrix44 perspective(const Matrix44::value_type p_fov, const Matrix44::value_type p_aspect_ratio, const Matrix44::value_type p_near, const Matrix44::value_type p_far)
    {
        return _perspective_rh01(p_fov, p_aspect_ratio, p_near, p_far);
    }

    inline Matrix44 _look_at_rh(const Vector3& p_eye, const Vector3& p_target, const Vector3& p_up)
    {
        using row_type = Vector3;

        const row_type f = normalized(p_target - p_eye);
        const row_type s = normalized(cross(f, p_up));
        const row_type u = cross(s, f);

        Matrix44 m(1);
        m.row0.x = s.x;
        m.row1.x = s.y;
        m.row2.x = s.z;

        m.row0.y = u.x;
        m.row1.y = u.y;
        m.row2.y = u.z;

        m.row0.z = -f.x;
        m.row1.z = -f.y;
        m.row2.z = -f.z;

        m.row3.x = -dot(s, p_eye);
        m.row3.y = -dot(u, p_eye);
        m.row3.z = dot(f, p_eye);

        return m;
    }

    inline Matrix44 look_at(const Vector3& p_eye, const Vector3& p_target, const Vector3& p_up)
    {
        return _look_at_rh(p_eye, p_target, p_up);
    }

}

#endif