#ifndef BASKET__QUATERNION_DEFINITION_HPP
#define BASKET__QUATERNION_DEFINITION_HPP

namespace basket
{
    // Quatertion = {x, y, z, w} or {q0, q1, q2, q3} or {q0, q}
    template<typename T>
    struct Quaternion_Template
    {
        using value_type = T;
        static constexpr size_t type_size = sizeof(T);
        union
        {
            value_type w;
            value_type q0;
        };

        union
        {
            value_type x;
            value_type q1;
        };

        union
        {
            value_type y;
            value_type q2;
        };

        union
        {
            value_type z;
            value_type q3;
        };

        constexpr Quaternion_Template(const T p_0, const T p_1, const T p_2, const T p_3): w(p_0), x(p_1), y(p_2), z(p_3) {}
        constexpr Quaternion_Template() = default;
        constexpr Quaternion_Template(const Quaternion_Template&) = default;
        constexpr Quaternion_Template(Quaternion_Template&&) = default;

        constexpr Quaternion_Template<T>& operator=(const Quaternion_Template&) = default;
        constexpr Quaternion_Template<T>& operator=(Quaternion_Template&&) = default;

        // static constexpr Quaternion_Template<T> identity{1, 0, 0, 0};
        
    };

    using Quaternion = Quaternion_Template<float>;

    static_assert(sizeof(Quaternion) == Quaternion::type_size * 4);
}

#endif