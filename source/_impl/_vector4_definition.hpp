#ifndef BASKET__VECTOR4_DEFINITION_HPP
#define BASKET__VECTOR4_DEFINITION_HPP

// #include <core/constraints.hpp>

namespace basket::bmath
{
    template<typename T>
    struct VectorXYZW_Template
    {
        
        using value_type = T;
        T x;
        T y;
        T z;
        T w;

        T& operator[](uint8_t index)
        {
            switch(index)
            {
                case 0:
                    return x;
                case 1:
                    return y;
                case 2:
                    return z;
                case 3:
                    return w;
            }

            return x;
        }

        const T& operator[](uint8_t index) const
        {
            switch(index)
            {
                case 0:
                    return x;
                case 1:
                    return y;
                case 2:
                    return z;
                case 3:
                    return w;
            }

            return x;
        }

        static const VectorXYZW_Template<T> one;
        static const VectorXYZW_Template<T> zero;

        constexpr VectorXYZW_Template<T>(T p_x, T p_y, T p_z, T p_w): x(p_x), y(p_y), z(p_z), w(p_w) {}
        // constexpr VectorXYZW_Template<T>(T p_value): x(p_value), y(p_value), z(p_value), w(p_value) {}
        constexpr VectorXYZW_Template<T>() = default;
        constexpr VectorXYZW_Template<T>(const VectorXYZW_Template<T>&) = default;
        constexpr VectorXYZW_Template<T>(VectorXYZW_Template<T>&&) = default;

        VectorXYZW_Template<T>& operator=(const VectorXYZW_Template<T>& p_other) = default;
        VectorXYZW_Template<T>& operator=(VectorXYZW_Template<T>&& p_other) = default;
        
    };

    static_assert(sizeof(VectorXYZW_Template<float>) == sizeof(float) * 4);

    template<typename T>
    const VectorXYZW_Template<T> VectorXYZW_Template<T>::one{1, 1, 1, 1};

    template<typename T>
    const VectorXYZW_Template<T> VectorXYZW_Template<T>::zero{0, 0, 0, 0};

    using Vector4 = VectorXYZW_Template<float>;
}

#endif