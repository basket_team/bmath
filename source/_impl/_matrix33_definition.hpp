#ifndef BASKET__MATRIX33_DEFINITION_HPP
#define BASKET__MATRIX33_DEFINITION_HPP

#include "_vector3_definition.hpp"

namespace basket::bmath
{
    template<typename T>
    struct _Matrix33_Template
    {
        using value_type = T;
        using row_type = VectorXYZ_Template<T>;

        row_type row0;
        row_type row1;
        row_type row2;

        row_type& operator[](int p_index)
        {
            switch(p_index)
            {
                case 0:
                    return row0;
                case 1:
                    return row1;
                case 2:
                    return row2;
            }

            return row0;
        }

        constexpr _Matrix33_Template() = default;
        constexpr _Matrix33_Template(const _Matrix33_Template<T>&) = default;
        constexpr _Matrix33_Template(_Matrix33_Template<T>&&) = default;

        constexpr _Matrix33_Template& operator=(const _Matrix33_Template<T>&) = default;
        constexpr _Matrix33_Template& operator=(_Matrix33_Template<T>&&) = default;

        constexpr _Matrix33_Template(const T p_value): row0(p_value, 0, 0), row1(0, p_value, 0), row2(0, 0, p_value) {}
        constexpr _Matrix33_Template(const row_type& p_row0, const row_type& p_row1, const row_type& p_row2):
            row0(p_row0), row1(p_row1), row2(p_row2)
            {}
        
        constexpr _Matrix33_Template(
            const value_type p_0, const value_type p_1, const value_type p_2,
            const value_type p_3, const value_type p_4, const value_type p_5,
            const value_type p_6, const value_type p_7, const value_type p_8
        ):
            row0{p_0, p_1, p_2},
            row1{p_3, p_4, p_5},
            row2{p_6, p_7, p_8}
        {}
        
        static const _Matrix33_Template<T> identity;
        static const _Matrix33_Template<T> one;
        
    };

    template<typename T>
    const _Matrix33_Template<T> _Matrix33_Template<T>::identity = {
        {1, 0, 0},
        {0, 1, 0},
        {0, 0, 1}
    };

    template<typename T>
    const _Matrix33_Template<T> _Matrix33_Template<T>::one = {
        {1, 1, 1},
        {1, 1, 1},
        {1, 1, 1}
    };

    using Matrix33 = _Matrix33_Template<float>;
}

#endif