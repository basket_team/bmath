#ifndef BASKET__QUATERNION_OPERATORS_HPP
#define BASKET__QUATERNION_OPERATORS_HPP

#include "_quaternion_definition.hpp"

constexpr basket::Quaternion operator*(const basket::Quaternion& p_left, const basket::Quaternion& p_right)
{
    // (p0 * q0 - p . q, p0 * q + q0 * p + p x q)
    return {
        p_left.w * p_right.w - p_left.x * p_right.x - p_left.y * p_right.y - p_left.z * p_right.z,
        p_left.w * p_right.x + p_left.x * p_right.w + p_left.y * p_right.z - p_left.z * p_right.y,
        p_left.w * p_right.y + p_left.y * p_right.w + p_left.z * p_right.x - p_left.x * p_right.z,
        p_left.w * p_right.z + p_left.z * p_right.w + p_left.x * p_right.y - p_left.y * p_right.x
    };
}

constexpr basket::Quaternion operator-(const basket::Quaternion& p_quat)
{
    return basket::Quaternion(p_quat.w, p_quat.x, p_quat.y, p_quat.z);
}

constexpr basket::Quaternion operator/(const basket::Quaternion& p_quat, const basket::Quaternion::value_type p_value)
{
    return basket::Quaternion(p_quat.w / p_value, p_quat.x / p_value, p_quat.y / p_value, p_quat.z / p_value);
}



#endif