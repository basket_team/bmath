#ifndef BASKET__VECTOR2_DEFINITION_HPP
#define BASKET__VECTOR2_DEFINITION_HPP

// #include <core/pch.hpp>
// #include <core/constraints.hpp>

#include <stdint.h>

namespace basket::bmath
{
    template<typename T>
    struct VectorXY_Template
    {
        using value_type = T;
        union
        {
            value_type x;
            value_type angle;
        };

        union
        {
            value_type y;
            value_type radius;
        };

        constexpr VectorXY_Template(T p_x, T p_y): x{p_x}, y{p_y} {}
        constexpr VectorXY_Template(const T p_value): VectorXY_Template(p_value, p_value) {}

        constexpr VectorXY_Template () = default;
        constexpr VectorXY_Template (const VectorXY_Template<T>&) = default;
        constexpr VectorXY_Template (VectorXY_Template<T>&&) = default;
        VectorXY_Template& operator=(const VectorXY_Template<T>&) = default;
        VectorXY_Template& operator=(VectorXY_Template<T>&&) = default;

    
        static const VectorXY_Template<T> zero;
        static const VectorXY_Template<T> one;
        static const VectorXY_Template<T> up;
        static const VectorXY_Template<T> down;
        static const VectorXY_Template<T> right;
        static const VectorXY_Template<T> left;

        

        // static constexpr VectorXY_Template<T> projection(const VectorXY_Template<T>& lhs, const VectorXY_Template<T>& rhs)
        // {
        //     float m = magnitude(rhs);
        //     return dot(lhs, rhs) * rhs / (m * m);
        // }

        // static constexpr VectorXY_Template<T> projection_normal(const VectorXY_Template<T>& lhs, const VectorXY_Template<T>& rhs)
        // {
        //     return dot(lhs, rhs) * rhs;
        // }
    };

    template<typename T>
    const VectorXY_Template<T> VectorXY_Template<T>::zero{0, 0};

    template<typename T>
    const VectorXY_Template<T> VectorXY_Template<T>::one{(T)1, (T)1};

    template<typename T>
    const VectorXY_Template<T> VectorXY_Template<T>::right{(T)1, 0};

    template<typename T>
    const VectorXY_Template<T> VectorXY_Template<T>::left{(T)-1, 0};

    template<typename T>
    const VectorXY_Template<T> VectorXY_Template<T>::up{0, (T)1};

    template<typename T>
    const VectorXY_Template<T> VectorXY_Template<T>::down{0, (T)-1};


    using Vector2 = VectorXY_Template<float>;
    using Vector2i64 = VectorXY_Template<int64_t>;
    using Vector2i = Vector2i64;

    static_assert(sizeof(Vector2) == sizeof(Vector2::value_type) * 2, "the size doesn't match");
}



#endif