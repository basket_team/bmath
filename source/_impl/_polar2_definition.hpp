#ifndef BASKET__POAR2_DEFINITION_HPP
#define BASKET__POAR2_DEFINITION_HPP

namespace basket::basket
{
    template<typename T>
    struct Polar2_Template
    {
        using value_type = T;
        static constexpr size_t type_size = sizeof(T);

        value_type angle;
        value_type radius;

        Polar2_Template(const T p_radius, const T p_angle): angle(p_angle), radius(p_radius) {};
        Polar2_Template() = default;
        Polar2_Template(const Polar2_Template<T>&) = default;
        Polar2_Template(Polar2_Template<T>&&) = default;

        Polar2_Template<T>& operator=(const Polar2_Template<T>&) = default;
        Polar2_Template<T>& operator=(Polar2_Template<T>&&) = default;
        
    };

    using Polar2 = Polar2_Template<float>;

    static_assert(sizeof(Polar2) == Polar2::type_size * 2);
}

#endif