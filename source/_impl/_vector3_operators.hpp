#ifndef BASKET__VECTOR3_OPERATORS_HPP
#define BASKET__VECTOR3_OPERATORS_HPP

#include "_vector3_definition.hpp"
#include "_matrix33_definition.hpp"
// #include "_vector3_functions.hpp"
// #include "_base_operators.hpp"

template<typename T, typename U>
constexpr bool operator== (const basket::VectorXYZ_Template<T>& rhs, const basket::VectorXYZ_Template<U>& lhs)
{
    return rhs.x == lhs.x && rhs.y == lhs.y && rhs.z == lhs.z;
}


template<typename T>
constexpr basket::VectorXYZ_Template<T> operator- (const basket::VectorXYZ_Template<T>& v)
{
    return basket::VectorXYZ_Template<T>(-v.x, -v.y, -v.z);
}


template<typename T>
constexpr basket::VectorXYZ_Template<T> operator+ (const basket::VectorXYZ_Template<T>& v)
{
    return v; // Dahhhh
}


template<typename T, typename U>
constexpr bool operator!= (const basket::VectorXYZ_Template<T>& rhs, const basket::VectorXYZ_Template<U>& lhs)
{
    return !(rhs == lhs);
}

template<typename T>
basket::VectorXYZ_Template<T> operator+ (const basket::VectorXYZ_Template<T>& rhs, const basket::VectorXYZ_Template<T>& lhs)
{
    return basket::VectorXYZ_Template<T>(rhs.x + lhs.x, rhs.y + lhs.y, rhs.z + lhs.z);
}


template<typename T, typename U>
basket::VectorXYZ_Template<T> operator- (const basket::VectorXYZ_Template<T>& rhs, const basket::VectorXYZ_Template<U>& lhs)
{
    return basket::VectorXYZ_Template<T>(rhs.x - lhs.x, rhs.y - lhs.y, rhs.z - lhs.z);
}



constexpr basket::Vector3 operator/ (const basket::Vector3& rhs, const typename basket::Vector3::value_type value)
{
    return basket::Vector3(rhs.x / value, rhs.y / value, rhs.z / value);
}



constexpr basket::Vector3 operator*(const basket::Vector3& rhs, const basket::Vector3::value_type value)
{
    return basket::Vector3(rhs.x * value, rhs.y * value, rhs.z * value);
}


constexpr basket::Vector3 operator*(const basket::Vector3::value_type value, const basket::Vector3& rhs)
{
    return basket::Vector3(rhs.x * value, rhs.y * value, rhs.z * value);
}

constexpr basket::Vector3 operator*(const basket::Matrix33& p_matrix, const basket::Vector3& p_vector)
{
    return {
        p_matrix.row0.x * p_vector.x + p_matrix.row0.y * p_vector.y + p_matrix.row0.z * p_vector.z,
        p_matrix.row1.x * p_vector.x + p_matrix.row1.y * p_vector.y + p_matrix.row1.z * p_vector.z,
        p_matrix.row2.x * p_vector.x + p_matrix.row2.y * p_vector.y + p_matrix.row2.z * p_vector.z
    };
}


template<typename T, typename U>
basket::VectorXYZ_Template<T>& operator+= (basket::VectorXYZ_Template<T>& rhs, const basket::VectorXYZ_Template<U>& lhs)
{
    rhs = rhs + lhs;
    return rhs;
}


template<typename T, typename U>
basket::VectorXYZ_Template<T>& operator-= (basket::VectorXYZ_Template<T>& rhs, const basket::VectorXYZ_Template<U>& lhs)
{
    rhs = rhs - lhs;
    return rhs;
}


template<typename T, typename U>
basket::VectorXYZ_Template<T>& operator/= (basket::VectorXYZ_Template<T>& rhs, const U value)
{
    rhs = rhs / value;
    return rhs;
}


template<typename T, typename U>
basket::VectorXYZ_Template<T>& operator*= (basket::VectorXYZ_Template<T>& rhs, const U value)
{
    rhs = rhs * value;
    return rhs;
}

#endif