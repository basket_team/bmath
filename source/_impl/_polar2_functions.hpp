#ifndef BASKET__POLAR2_FUNCTIONS_HPP
#define BASKET__POLAR2_FUNCTIONS_HPP

#include "_polar2_definition.hpp"
#include "_vector2_definition.hpp"
#include "_vector2_operators.hpp"
// #include <core/pch.hpp>

struct Vector2;

namespace basket::bmath
{
    inline Vector2 to_vector2(const Polar2& p_polar)
    {
        Vector2 uv(cos(p_polar.angle), sin(p_polar.angle));
        return p_polar.radius * uv;
    }
}

#endif