#ifndef BASKET_MATRIX22_HPP
#define BASKET_MATRIX22_HPP

#include "_impl/_matrix22_definition.hpp"
#include "_impl/_matrix22_operators.hpp"
#include "_impl/_matrix22_functions.hpp"
#include "_impl/_matrix22_stream.hpp"

#endif